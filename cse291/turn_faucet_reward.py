
from mani_skill2.envs.pick_and_place.pick_cube import PickCubeEnv
from mani_skill2.sensors.camera import CameraConfig
from mani_skill2.utils.registration import register_env
from mani_skill2.utils.sapien_utils import look_at
from mani_skill2.envs.misc.turn_faucet import TurnFaucetEnvSubset

import numpy as np
import sapien.core as sapien

@register_env("TurnFaucet-v3")
class CustomPegInsertion(TurnFaucetEnvSubset):
    def compute_dense_reward(self, info, **kwargs):
        reward = 0.0

        if info["success"]:
            return 10.0

        distance = self._compute_distance()
        reward += 1 - np.tanh(distance * 5.0)

        # is_contacted = any(self.agent.check_contact_fingers(self.target_link))
        # if is_contacted:
        #     reward += 0.25

        angle_diff = self.target_angle - self.current_angle
        turn_reward_1 = 3 * (1 - np.tanh(max(angle_diff, 0) * 2.0))
        reward += turn_reward_1

        delta_angle = angle_diff - self.last_angle_diff
        if angle_diff > 0:
            turn_reward_2 = -np.tanh(delta_angle * 2)
        else:
            turn_reward_2 = np.tanh(delta_angle * 2)
        turn_reward_2 *= 5
        reward += turn_reward_2
        self.last_angle_diff = angle_diff

        return reward
